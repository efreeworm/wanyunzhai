//app.js
App({
  onLaunch: function() {
    // 展示本地存储能力
    // var logs = wx.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // wx.setStorageSync('logs', null)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
    var that = this
    wx.getSystemInfo({
      success: function(res) {
        // var windowWidth = res.windowWidth;
        // var windowHeight = res.windowHeight;
        console.log(res.windowHeight)
        that.globalData.windowWidth = res.windowWidth;
        that.globalData.windowHeight = res.windowHeight;

      }
    })
  },
  globalData: {
    userInfo: null,
    imgUrl: 'https://gitlab.com/efreeworm/wanyunzhai/raw/master/images/',
    windowWidth: null,
    windowHeight: null,
    menu:['花鸟','人物','山水'],
    list: [{
      image: 'furongjinji.png',
      images: ['furongjinji138cm×68cm_750.jpg', 'furongjinji138cm×68cm_origin.jpg'],
      name: '芙蓉锦鸡', theme: '花鸟', size: '138cm x 68cm'
    },

    {
      image: 'lizhiji138cm×68cm.png',
      images: ['lizhiji138cm×68cm_750.jpg', 'lizhiji138cm×68cm_origin.jpg'],
      name: '荔枝鸡', theme: '花鸟', size: '138cm x 68cm'
    },

    {
      image: 'longdongduixue66cm×44cm.png',
      images: ['longdongduixue66cm×44cm_750.jpg', 'longdongduixue66cm×44cm_origin.jpg'],
      name: '隆冬堆雪', theme: '人物', size: '66cm x 44cm'
    },
    {
      image: 'shanshui2.png',
      images: ['shangshui2_750.jpg', 'shangshui2_origin.jpg'],
      name: '山水', theme: '山水', size: ''
    },

    {
      image: 'du.png',
      images: ['du138cm×68cm_750.jpg', 'du138cm×68cm_origin.jpg'],
      name: '读', theme: '人物', size: '138cm x 68cm'
    },
    {
      image: 'gengtianniu.png',
      images: ['gengtianniu54cm×75cm_750.jpg', 'gengtianniu54cm×75cm_origin.jpg'],
      name: '耕牛图', theme: '人物', size: '54cm x 75cm'
    },


    {
      image: 'jixiapudie.png',
      images: ['jixiapudie66cm×44cm_750.jpg', 'jixiapudie66cm×44cm_origin.jpg'],
      name: '季夏扑蝶', theme: '人物', size: '66cm x 44cm'
    },

    {
      image: 'ju.png',
      images: ['yu55cm×77cm_750.jpg', 'ju55cm×77cm_origin.jpg'],
      name: '菊', theme: '花鸟', size: '55cm x 77cm'
    },
    {
      image: 'ju1.png',
      images: ['ju68cm×33cm_750.jpg', 'ju68cm×33cm_origin.jpg'],
      name: '菊', theme: '花鸟', size: '68cm x 33cm'
    },
    {
      image: 'ju2.png',
      images: ['ju_750.jpg', 'ju_origin.jpg'],
      name: '菊', theme: '花鸟', size: ''
    },
    {
      image: 'danguipiaoxiang.png',
      images: ['danguipiaoxiang55cm×77cm_750.jpg', 'danguipiaoxiang55cm×77cm_origin.jpg'],
      name: '丹桂飘香', theme: '花鸟', size: '55cm x 77cm'
    },
    {
      image: 'bainiaochaofeng.png',
      images: ['bainiaochaofeng66cm×134cm_750.jpg', 'bainiaochaofeng-66cm×134cm_origin.jpg'],
      name: '百鸟朝凤', theme: '花鸟', size: '66cm x 134cm'
    },
    {
      image: 'geng.png',
      images: ['geng138cm×68cm_750.jpg', 'geng138cm×68cm_origin.jpg'],
      name: '耕', theme: '人物', size: '138cm x 68cm'
    },

    {
      image: 'baicai.png',
      images: ['baicai_750.jpg', 'baicai_origin.jpg'],
      name: '白菜', theme: '花鸟', size: ''
    },


    {
      image: 'shanghetu92cm×175cm.png',
      images: ['shanghetu92cm×175cm_750.jpg', 'shanghetu92cm×175cm_origin.jpg'],
      name: '赏荷图', theme: '人物', size: '92cm x 175cm'
    },
    {
      image: 'lan.68cmx33cm.png',
      images: ['lan68cm×33cm_750.jpg', 'lan68cm×33cm_origin.jpg'],
      name: '兰', theme: '花鸟', size: '68cm x 33cm'
    },

    {
      image: 'fengmian.png',
      images: ['yucuidanxiao_750.jpg', 'yucuidanxiao_origin.jpg'],
      name: '羽翠丹霞', theme: '花鸟', size: ''
    },
    {
      image: 'lan.55cm×77cm.png',
      images: ['lan55cm×77cm_750.jpg', 'ju55cm×77cm_origin.jpg'],
      name: '兰', theme: '花鸟', size: '55cm x 77cm'
    },

    {
      image: 'lan.png',
      images: ['lan_750.jpg', 'lan_origin.jpg'],
      name: '兰', theme: '花鸟', size: ''
    },
    {
      image: 'mengchunxichong66cm×44cm.png',
      images: ['mengchunxichong66cm×44cm_750.jpg', 'mengchunxichong66cm×44cm_origin.jpg'],
      name: '孟春戏虫', theme: '人物', size: '66cm x 44cm'
    },

    {
      image: 'mei68cm×33cm.png',
      images: ['mei68cm×33cm_750.jpg', 'mei68cm×33cm_origin.jpg'],
      name: '傲霜迎春', theme: '花鸟', size: '68cm x 33cm'
    },

    {
      image: 'mei55cm×77cm.png',
      images: ['mei55cm×77cm_750.jpg', 'mei55cm×77cm_origin.jpg'],
      name: '梅', theme: '花鸟', size: '55cm x 77cm'
    },
    {
      image: 'pipaxiaoji68cm×33cm.png',
      images: ['pipaxiaoji68cm×33cm_750.jpg', 'pipaxiaoji68cm×33cm_origin.jpg'],
      name: '枇杷小鸡', theme: '花鸟', size: '68cm x 33cm'
    },
    {
      image: 'pinguanqunfang.png',
      images: ['pinguanqunfang_750.jpg', 'pinguanqunfang_origin.jpg'],
      name: '品冠群芳', theme: '花鸟', size: ''
    },

    {
      image: 'shinv1.33cm×33cm.png',
      images: ['shinv1.33cm×33cm_750.jpg', 'shinv1.33cm×33cm_origin.jpg'],
      name: '仕女', theme: '人物', size: '33cm x 33cm'
    },
    {
      image: 'shenshanwusuke55cm×77cm.png',
      images: ['shenshanwusuke55cm×77cm_750.jpg', 'shenshanwusuke55cm×77cm_origin.jpg'],
      name: '深山无俗客', theme: '人物', size: '55cm x 77cm'
    },
    {
      image: 'shanshui1.png',
      images: ['shangshui1_750.jpg', 'shangshui1_origin.jpg'],
      name: '山水', theme: '山水', size: ''
    },
    {
      image: 'quezaohanlin138cm×68cm.png',
      images: ['quezaohanlin138cm×68cm_750.jpg', 'quezaohanlin138cm×68cm_origin.jpg'],
      name: '雀噪寒林', theme: '花鸟', size: '138cm x 68cm'
    },
    {
      image: 'nongjiaxiaoqu54cm×75cm.png',
      images: ['nongjiaxiaoqu54cm×75cm_750.jpg', 'nongjiaxiaoqu54cm×75cm_origin.jpg'],
      name: '农家小趣', theme: '花鸟', size: '54cm x 75cm'
    },
    {
      image: 'mei.png',
      images: ['mei_750.jpg', 'mei_origin.jpg'],
      name: '梅', theme: '花鸟', size: ''
    },

    {
      image: 'qiao138cm×68cm.png',
      images: ['qiao138cm×68cm_750.jpg', 'qiao138cm×68cm_origin.jpg'],
      name: '樵', theme: '人物', size: '138cm x 68cm'
    },
    {
      image: 'jinyu.png',
      images: ['jinyu54cm×75cm_750.jpg', 'jinyu54cm×75cm_origin.jpg'],
      name: '金鱼', theme: '花鸟', size: '54cm x 75cm'
    },

    {
      image: 'xishangmeishao.png',
      images: ['xishangmeishao_750.jpg', 'xishangmeishao_origin.jpg'],
      name: '喜上眉梢', theme: '花鸟', size: ''
    },
    {
      image: 'niannujiao55cm×77cm.png',
      images: ['niannujiao55cm×77cm_750.jpg', 'niannujiao55cm×77cm_origin.jpg'],
      name: '念奴娇', theme: '花鸟', size: '55cm x 77cm'
    },
    {
      image: 'zl000009.png',
      images: ['jinyu_750.jpg', 'jinyu_origin.jpg'],
      name: '金鱼图', theme: '花鸟', size: ''
    },
    {
      image: 'zhu.png',
      images: ['zhu_750.jpg', 'zhu_origin.jpg'],
      name: '竹', theme: '花鸟', size: ''
    },
    {
      image: 'zhu68cm×33cm.png',
      images: ['zhu68cm×33cm_750.jpg', 'zhu68cm×33cm_origin.jpg'],
      name: '竹', theme: '花鸟', size: '68cm x 33cm'
    },
    {
      image: 'jinfeng.png',
      images: ['jinfeng54cm×75cm_750.jpg', 'jinfeng54cm×75cm_origin.jpg'],
      name: '劲风', theme: '花鸟', size: '54cm x 75cm'
    },

    {
      image: 'zhu55cm×77cm.png',
      images: ['zhu55cm×77cm_750.jpg', 'zhu55cm×77cm_origin.jpg'],
      name: '竹', theme: '花鸟', size: '55cm x 77cm'
    },
    {
      image: 'zhongqiuzhuoying66cm×44cm.png',
      images: ['zhongqiuzhuoying66cm×44cm_750.jpg', 'zhongqiuzhuoying66cm×44cm_origin.jpg'],
      name: '仲秋捉鹰', theme: '人物', size: '66cm x 44cm'
    },
    {
      image: 'yu138cm×68cm.png',
      images: ['yu138cm×68cm_750.jpg', 'yu138cm×68cm_origin.jpg'],
      name: '渔', theme: '人物', size: '138cm x 68cm'
    },

    {
      image: 'shinv2.33cm×33cm.png',
      images: ['shinv2.33cm×33cm_750.jpg', 'shinv2.33cm×33cm_origin.jpg'],
      name: '仕女', theme: '人物', size: '33cm x 33cm'
    },
    {
      image: 'shiliuliyu132cm×66cm.png',
      images: ['shiliuliyu.132cm×66cm_750.jpg', 'shiliuliyu.132cm×66cm_origin.jpg'],
      name: '石榴鲤鱼', theme: '花鸟', size: '132cm x 66cm'
    },
    {
      image: 'jiaoxue.png', images: ['jiaoxue.png', 'jiaoxue.png'],
      name: '教授画技', theme: '人物', size: ''
    }
    ]
  }
})