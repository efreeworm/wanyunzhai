const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl: app.globalData.imgUrl,
    list: app.globalData.list,
    originData: app.globalData.list,
    menu: false,
    index: "scale(1) translateX(-500px)",
    mid:-1//菜单ID
  },

  details(e) {
    let index = e.currentTarget.dataset.id;
    //let info = this.data.list[index]
    wx.navigateTo({
      url: 'details1?index=' + index+'&mid='+this.data.mid,
    })
  },
  showMenu() {
    this.setData({
      menu: !this.data.menu,
      index: !this.data.menu ? "scale(1) translateX(0)" : "scale(1) translateX(-500px)"
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.showLoading({
      title: '加载中',
    })
  },

  /**
   * 过滤类型
   */
  setFliter: function(e) {
    let val = app.globalData.menu[e.currentTarget.dataset.id]
    var data = this.data.originData
    let ds = []
    for (let i = 0; i < data.length; i++) {
      let a = data[i]
      if (a.theme == val)
        ds.push(a)
    }

    this.setData({
      list: ds,
      mid:e.currentTarget.dataset.id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    wx.hideLoading()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})