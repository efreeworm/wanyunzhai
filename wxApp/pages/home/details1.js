const app = getApp();
const imageUtil = require('../../utils/util.js');
var interval = ""; // 记录/清理 时间记录
var time = 0; //  时间记录，用于滑动时且时间小于1s则执行左右滑动
Page({

  /**
   * 页面的初始数据
   */
  data: {
    picture: '',
    info: {},
    imgUrl: app.globalData.imgUrl,
    windowHeight: app.globalData.windowHeight,
    windowWidth: app.globalData.windowWidth,
    index: "scale(1) translateX(0)",
    flag: true,//是否需要展开
    start: 0,//开始的点坐标
    end: 0,//结束的点的坐标
    distance: "",//拖动的距离
    max: app.globalData.windowWidth - 60,//拖动的最大值
    isdrag: false,//是否经过了拖动，增加是否拖动的布尔值是用来保证我单击展开的正确性
    direct: 0, //左右移动的方向
    listIndex: -1,
    postion: 0,//top的位置
    list: [],
    mid: -1,
  },
  call() {
    wx.makePhoneCall({
      phoneNumber: '13648025360'
    })
  },
  preview(e) {
    wx.previewImage({
      urls: [this.data.imgUrl + this.data.info.images[1]],
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    time = 0
    let index = parseInt(options.index)
    let mid = parseInt(options.mid==undefined?-1:options.mid)
    let list = []
    if (mid == -1) {
      list = app.globalData.list
    }
    else {
      let data = app.globalData.list
      let val = app.globalData.menu[mid]
      for (let i = 0; i < data.length; i++) {
        let a = data[i]
        if (a.theme == val)
          list.push(a)
      }
    }
    let info = list[index]

    let url = this.data.imgUrl
    this.setData({
      info: info,
      picture: url + info.images[0],
      listIndex: index,
      mid: mid,
      list: list
    })
    wx.setNavigationBarTitle({
      title: info.name
    })
    // console.log(this.data.picture)
  },
  imageLoad: function (e) {
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight
    })
    wx.hideLoading()
  },

  tab_start: function (e) {
    this.setData({
      start: e.touches["0"].pageX,//获取一开始点的坐标
      isdrag: false
    })
    interval = setInterval(function () {
      time++;
    }, 50);
  },
  tab_move: function (e) {
    this.data.isdrag = true//确认经过拖动
    this.data.end = e.touches["0"].pageX//获取结束点的坐标
    this.data.distance = this.data.end - this.data.start//求拖动的距离
    this.setData({
      end: e.touches[0].pageX,
      distance: this.data.end - this.data.start
    })
    //如果拖动的距离大于最大值，还是最大值
    //if(this.data.start-this.data.distance)
    if (this.data.distance < this.data.max) {
      // this.data.index = "scale(1) translate(" +this.data.distance+",0)"
      let distance = this.data.distance + "px"
      this.setData({
        index: "scale(1) translateX(" + distance + ")",
        // postion: this.data.distance
      })
      //  console.log(this.data.index)
    }
  },
  tab_end: function (e) {
    this.data.distance = this.data.end - this.data.start
    let direct = this.data.distance - this.data.start
    console.log(time, this.data.postion, this.data.distance)
    if (time >= 2 && this.data.postion == 0 && this.data.distance < 0 && Math.abs(this.data.distance) > 60) {
      if (this.data.listIndex < this.data.list.length-1) {
         this.details(this.data.listIndex)
      }
      else {
        this.details(-1)
      }
    }


    //console.log(direct, this.data.distance, this.data.distance <= (this.data.windowWidth / 3) && this.data.isdrag)
    // if(this.data.distance)
    //拖动结束判断是否经过拖动，而且，拖动距离是否大于最大值。没有最大值收缩回去
    if (this.data.distance <= (this.data.windowWidth / 3) && this.data.isdrag) {
      this.setData({
        flag: true,
        index: "scale(1) translateX(0)",
        postion: 0
      })

    }
    else if (direct > 0) {
      //console.log(direct, "scale(1) translateX(" + this.data.max + "px)")
      this.setData({
        flag: true,
        index: "scale(1) translateX(" + this.data.max + "px)",
        postion: this.data.max
      })
    }
    clearInterval(interval); // 清除setInterval
    time = 0;

    // else if (this.data.isdrag) {
    //   this.data.flag = false
    // }
    this.setData({
      isdrag: false,
      distance: 0,
    })
    //console.log("postion ",this.data.postion == 0 && this.data.distance < 0)

  },
  details(e) {
    let index = e + 1
    wx.redirectTo({
      url: 'details1?index=' + index + '&mid=' + this.data.mid,
    })
  },
  func: function () {
    //console.log('func')
    if (this.data.flag) {
      this.setData({
        index: "scale(1) translateX(30%)",
        flag: false
      })
    } else {
      this.setData({
        index: "scale(1) translateX(0)",
        flag: true
      })
    }
  },
  onUnload: function () {
    clearInterval(interval);
  },
  onShareAppMessage: function () {
    return {
      title: this.data.info.name,
      imageUrl: this.data.picture
    }
  }
})