var imageUtil = require('../../utils/util.js');
const app = getApp();
var time = 0;

var touchDot = 0; //触摸时的原点
var touchDotY = 0; //触摸时的原点
var time = 0; //  时间记录，用于滑动时且时间小于1s则执行左右滑动
var interval = ""; // 记录/清理 时间记录
var nth = 0; // 设置活动菜单的index
var nthMax = 5; //活动菜单的最大个数
var tmpFlag = true; // 判断左右华东超出菜单最大值时不再执行滑动事件
var p1 = 0;
var dirct = 0;
var diff = 0;
var diffY = 0;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl: app.globalData.imgUrl,
    windowHeight: app.globalData.windowHeight,
    windowWidth: app.globalData.windowWidth,
    picture: '',
    info: {},
    imagewidth: 0, //缩放后的宽 
    imageheight: 0, //缩放后的高 
    post: -600
  },
  call() {
    wx.makePhoneCall({
      phoneNumber: '13648025360'
    })
  },
  preview(e) {
    wx.previewImage({
      urls: [this.data.imgUrl + this.data.info.images[1]],
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.showLoading({
      title: '加载中',
    })
    let info = JSON.parse(options.info)
    let url = this.data.imgUrl
    this.setData({
      info: info,
      picture: url + info.images[0]
    })
    wx.setNavigationBarTitle({
      title: info.name
    })
    // console.log(this.data.picture)
  },
  imageLoad: function(e) {
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight
    })
    wx.hideLoading()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    // console.log('onReady', this.data)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // console.log('onShow',this.data)
    // console.log(touchDot, touchDotY, diff, diffY, dirct)
  },
  // 触摸开始事件
  touchStart: function(e) {
    p1 = this.data.post
    touchDot = e.touches[0].pageX; // 获取触摸时的原点
    touchDotY = e.touches[0].pageY; // 获取触摸时的原点
    // 使用js计时器记录时间    
    interval = setInterval(function() {
      time++;
    }, 100);
  },
  // 触摸移动事件
  touchMove: function(e) {
    var touchMove = e.touches[0].pageX;
    var touchMoveY = e.touches[0].pageY;
    diffY = touchMoveY - touchDotY
    diff = touchMove - touchDot
    // console.log("touchMove:" + touchMove + " touchDot:" + touchDot + " diff:" + (diff));
    // 向左滑动   
    if (diff <= -40 && time < 10) {
      dirct = -1
      this.setData({
        post: p1 + diff
      })
      // console.log(dirct, this.data.post)
      return
      if (tmpFlag && nth < nthMax) { //每次移动中且滑动时不超过最大值 只执行一次
        var tmp = this.data.menu.map(function(arr, index) {
          tmpFlag = false;
          if (arr.active) { // 当前的状态更改
            nth = index;
            ++nth;
            arr.active = nth > nthMax ? true : false;
          }
          if (nth == index) { // 下一个的状态更改
            arr.active = true;
            name = arr.value;
          }
          return arr;
        })
        // this.getNews(name); // 获取新闻列表
        // this.setData({ menu: tmp }); // 更新菜单
      }
    }
    // 向右滑动
    if (diff >= 40 && time < 10) {
      dirct = 1
      this.setData({
        post: p1 + diff
      })

      // console.log(this.data.post, dirct)
      return
      if (tmpFlag && nth > 0) {

        nth = --nth < 0 ? 0 : nth;
        var tmp = this.data.menu.map(function(arr, index) {
          tmpFlag = false;
          arr.active = false;
          // 上一个的状态更改
          if (nth == index) {
            arr.active = true;
            name = arr.value;
          }
          return arr;
        })
        // this.getNews(name); // 获取新闻列表
        // this.setData({ menu: tmp }); // 更新菜单
      }
    }
    // touchDot = touchMove; //每移动一次把上一次的点作为原点（好像没啥用）
  },
  // 触摸结束事件
  touchEnd: function(e) {
    if (time < 10 && Math.abs(diffY) > 40) {
      this.preview()
      return;
    }
    var post = this.data.post
    // console.log('touchEnd', post, dirct, diff,time,diffY)
    clearInterval(interval); // 清除setInterval
    time = 0;
    tmpFlag = true; // 回复滑动事件
    post = p1 + diff
    if (Math.abs(diff) > 30) {
      // console.log('diff > 40 ')
      if ((post > -598 && dirct == -1) || (post < -605 && dirct == 1)) {
        this.setData({
          post: -600,
          dirct: 0
        })
      } else {
        if (dirct == -1) {
          this.setData({
            post: -600 - 600
          })
        } else if (dirct == 1) {
          this.setData({
            post: 1
          })
        }
      }
      // console.log(this.data.post)
    } else {
      // console.log('diff <= 40 ')
      if (post <= -520 && post > -640) {
        this.setData({
          post: -600,
          dirct: 0
        })
      } else if (post <= 1 && post > -40) {
        this.setData({
          post: 1
        })
      } else {
        this.setData({
          post: -1200
        })
      }
      // console.log(this.data.post)
    }
    diff = diffY = 0
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    // console.log('onHide')
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    // console.log('onUnload')
    time = 0;
    touchDot = 0; //触摸时的原点
    touchDotY = 0; //触摸时的原点
    time = 0; //  时间记录，用于滑动时且时间小于1s则执行左右滑动
    interval = ""; // 记录/清理 时间记录
    nth = 0; // 设置活动菜单的index
    nthMax = 5; //活动菜单的最大个数
    tmpFlag = true; // 判断左右华东超出菜单最大值时不再执行滑动事件
    p1 = 0;
    dirct = 0;
    diff = 0;
    diffY = 0;
    // console.log(this.data)
  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})